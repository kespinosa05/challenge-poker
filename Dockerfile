FROM openjdk:17-alpine

MAINTAINER Kenny Lopez "kespinosa05@gmail.com"

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

EXPOSE 8080

WORKDIR /api

COPY build/libs/poker-game-1.0.0-SNAPSHOT.jar /api/app.jar

ENTRYPOINT exec java -jar app.jar
