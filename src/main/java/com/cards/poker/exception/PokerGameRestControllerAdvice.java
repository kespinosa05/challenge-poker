package com.cards.poker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class PokerGameRestControllerAdvice {

    @ResponseBody
    @ExceptionHandler(PokerGameBaseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse pokerGameBaseException(PokerGameBaseException ex) {
        return ErrorResponse.builder()
                .errorType(ex.getClass().getSimpleName())
                .message(ex.getMessage())
                .build();
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorResponse genericExceptionHandler(Exception ex) {
        return ErrorResponse.builder()
                .errorType(ex.getClass().getSimpleName())
                .message(ex.getMessage())
                .build();
    }


}
