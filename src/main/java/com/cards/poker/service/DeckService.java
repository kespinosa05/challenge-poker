package com.cards.poker.service;

import com.cards.poker.exception.DeckNotFoundException;
import com.cards.poker.model.Card;
import com.cards.poker.model.Deck;
import com.cards.poker.model.FaceType;
import com.cards.poker.model.SuitType;
import com.cards.poker.model.event.PokerGameEntityType;
import com.cards.poker.model.event.PokerGameEvent;
import com.cards.poker.util.LocalConcurrentHashMap;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.cards.poker.model.event.PokerGameEntityType.DECK;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

@Service
@AllArgsConstructor
public class DeckService {
    private final ApplicationEventPublisher eventPublisher;

    private final LocalConcurrentHashMap<UUID, Deck> decks = new LocalConcurrentHashMap<>();

    private final MetricService metricService;

    private void publishEvent(List<PokerGameEntityType> entities, String action) {
        eventPublisher.publishEvent(new PokerGameEvent(this, entities, action, Collections.emptyMap()));
    }

    public Deck getDeck(UUID id) {
        Deck deck = decks.get(id);
        if (deck == null) {
            throw new DeckNotFoundException();
        }
        return deck;
    }

    public Deck createNewDeck() {
        publishEvent(List.of(DECK), "Deck created");
        UUID uuid = UUID.randomUUID();
        Deck deck = Deck.builder()
                .id(uuid)
                .cards(DeckService.generateDeckCards())
                .build();
        decks.put(uuid, deck);
        metricService.deckCreated();
        return deck;
    }

    /**
     * A deck card is Fifty-two playing cards in four suits: hearts, spades, clubs, and diamonds, with face values of Ace,
     * 2-10, Jack Queen, and King.
     */
    public static Set<Card> generateDeckCards() {
        return stream(SuitType.values())
                .flatMap(s -> stream(FaceType.values()).map(f -> Card.builder()
                        .suit(s)
                        .face(f)
                        .build()))
                .collect(toSet());
    }

}
