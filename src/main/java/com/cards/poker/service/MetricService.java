package com.cards.poker.service;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MetricService {

    static final String COUNTER_GAME_CREATED = "counter.game.created";
    static final String COUNTER_GAME_PLAYERS_ADDED = "counter.game.player.added";
    static final String COUNTER_GAME_PLAYERS_REMOVED = "counter.game.player.removed";

    static final String COUNTER_GAME_DEAL_CARD = "counter.game.dealcard";

    static final String COUNTER_DECK_CREATED = "counter.deck.created";
    public static final String GAME_NAME = "gameName";
    public static final String PLAYER_NAME = "playerName";

    private final MeterRegistry registry;

    public void gameCreated() {
        registry.counter(COUNTER_GAME_CREATED).increment();
    }

    public void playerAdded(String gameName, String playerName) {
        registry.counter(COUNTER_GAME_PLAYERS_ADDED, List.of(Tag.of(GAME_NAME, gameName), Tag.of(PLAYER_NAME, playerName))).increment();
    }

    public void playerRemoved(String gameName, String playerName) {
        registry.counter(COUNTER_GAME_PLAYERS_REMOVED, List.of(Tag.of(GAME_NAME, gameName), Tag.of(PLAYER_NAME, playerName))).increment();
    }

    public void deal(String gameName, String playerName, Integer quantity) {
        registry.counter(COUNTER_GAME_DEAL_CARD, List.of(Tag.of(GAME_NAME, gameName), Tag.of(PLAYER_NAME, playerName))).increment(quantity);
    }

    public void deckCreated() {
        registry.counter(COUNTER_DECK_CREATED).increment();
    }

}
