package com.cards.poker.service;

import com.cards.poker.dto.PokerGameEventDTO;
import com.cards.poker.model.event.PokerGameEntityType;
import com.cards.poker.model.event.PokerGameEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PokerGameChangeListener {

    private final Set<PokerGameEvent> eventsHistory = new LinkedHashSet<>();

    @EventListener
    public void handleCustomEvent(PokerGameEvent event) {
        eventsHistory.add(event);
    }

    public List<PokerGameEventDTO> getEventsPerEntityType(PokerGameEntityType entity) {
        return eventsHistory.stream().filter(e -> e.getEntities().contains(entity)).map(e -> PokerGameEventDTO.builder()
                .date(LocalDateTime.ofInstant(Instant.ofEpochMilli(e.getTimestamp()),
                        TimeZone.getDefault().toZoneId()))
                .action(e.getAction())
                .details(e.getDetails())
                .build()).collect(Collectors.toList());
    }

}
