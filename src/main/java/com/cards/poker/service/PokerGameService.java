package com.cards.poker.service;

import com.cards.poker.dto.CardsUnDealtPerSuitDTO;
import com.cards.poker.dto.PlayerTotalValueDTO;
import com.cards.poker.exception.*;
import com.cards.poker.model.*;
import com.cards.poker.model.event.PokerGameEntityType;
import com.cards.poker.model.event.PokerGameEvent;
import com.cards.poker.util.LocalConcurrentHashMap;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

import static com.cards.poker.model.event.PokerGameEntityType.*;

@Service
@AllArgsConstructor
public class PokerGameService {
    private static final Random RANDOM = new SecureRandom();
    public static final String GAME_NAME = "gameName";
    public static final String PLAYER_NAME = "playerName";

    private final LocalConcurrentHashMap<String, PokerGame> pokerGames = new LocalConcurrentHashMap<>();

    private final MetricService metricService;

    private final DeckService deckService;

    private final ApplicationEventPublisher eventPublisher;

    private void publishEvent(List<PokerGameEntityType> entities, String action, Map<String, Object> details) {
        eventPublisher.publishEvent(new PokerGameEvent(this, entities, action, details));
    }

    private PokerGame getGame(String name) {
        if (pokerGames.containsKey(name)) {
            return pokerGames.get(name);
        } else {
            throw new GameNotFoundException();
        }
    }

    private boolean existGame(String name) {
        return pokerGames.containsKey(name);
    }

    public PokerGame createGame(String name) {
        var gameExist = existGame(name);
        if (gameExist) {
            throw new GameAlreadyExistException();
        }
        var game = PokerGame.builder()
                .name(name)
                .build();
        pokerGames.put(name, game);
        publishEvent(List.of(GAME), "Game created", Map.of(GAME_NAME, name));
        metricService.gameCreated();
        return game;
    }

    public void deleteGame(String name) {
        if (pokerGames.containsKey(name)) {
            pokerGames.remove(name);
            publishEvent(List.of(GAME), "Game deleted", Map.of(GAME_NAME, name));
        } else {
            throw new GameNotFoundException();
        }
    }


    public void addDeckToGame(String gameName, UUID deckId) {
        var game = getGame(gameName);
        if (game.getDecks().stream().noneMatch(d -> d.getId().equals(deckId))) {
            var deck = deckService.getDeck(deckId);
            game.getDecks().add(deck);
            publishEvent(List.of(GAME, DECK), "Deck added", Map.of(GAME_NAME, gameName));
        } else {
            throw new DeckAlreadyExistInPokerGameException();
        }
    }

    private boolean existPlayer(PokerGame game, String name) {
        return game.getPlayers().stream()
                .anyMatch(p -> p.getName().equals(name));
    }

    private Player getPlayer(PokerGame game, String name) {
        return game.getPlayers().stream()
                .filter(p -> p.getName().equals(name)).findFirst()
                .orElseThrow(PlayerNotFoundException::new);
    }

    public Player addPlayer(String gameName, String playerName) {
        var game = getGame(gameName);
        var existPlayer = existPlayer(game, playerName);
        if (existPlayer) {
            throw new PlayerAlreadyExistException();
        }
        var player = getPlayer(playerName);
        game.getPlayers().add(player);
        publishEvent(List.of(GAME, PLAYER), "Player added", Map.of(GAME_NAME, gameName, PLAYER_NAME, player.getName()));
        metricService.playerAdded(gameName, playerName);
        return player;
    }

    private Player getPlayer(String playerName) {
        return Player.builder().name(playerName).build();
    }

    public void removePlayer(String gameName, String playerName) {
        var game = getGame(gameName);
        var deleted = game.getPlayers().removeIf(g -> g.getName().equals(playerName));
        if (!deleted) {
            throw new PlayerNotFoundException();
        } else {
            publishEvent(List.of(GAME, PLAYER), "Player deleted", Map.of(GAME_NAME, gameName, PLAYER_NAME, playerName));
            metricService.playerRemoved(gameName, playerName);
        }
    }

    public void dealCards(String gameName, String playerName, Integer quantity) {
        var game = getGame(gameName);
        var player = getPlayer(game, playerName);

        synchronized (game) {
            var deck = getRandomDeck(game);
            var cards = new ArrayList<>();
            var amountCardsToDeal = quantity;
            while (amountCardsToDeal-- > 0) {
                var card = deck.retriveCard();
                if (card != null) {
                    cards.add(card);
                    player.getHand().add(card);
                }
            }
            metricService.deal(gameName, playerName, quantity);
            publishEvent(List.of(GAME, PLAYER, DECK), "Deal Cards", Map.of(GAME_NAME, gameName, PLAYER_NAME, playerName, "cards", cards));
        }
    }

    private static Deck getRandomDeck(PokerGame game) {
        if (game.getDecks().isEmpty()) {
            throw new DeckNotFoundException();
        }
        var randomDeckIndex = RANDOM.nextInt(game.getDecks().size());
        return game.getDecks().stream().skip(randomDeckIndex).findFirst().orElseThrow();
    }

    public List<Card> getPlayerHand(String gameName, String playerName) {
        var game = getGame(gameName);
        return getPlayer(game, playerName).getHand().stream().toList();
    }

    public List<PlayerTotalValueDTO> totalRankPerPlayer(String gameName) {
        var game = getGame(gameName);
        return game.getPlayers().stream()
                .map(p -> new PlayerTotalValueDTO(p.getName(), p.getTotalValue()))
                .sorted((e1, e2) -> e2.getTotalValue().compareTo(e1.getTotalValue()))
                .collect(Collectors.toList());
    }

    public List<CardsUnDealtPerSuitDTO> cardsUnDealtPerSuit(String gameName) {
        var game = getGame(gameName);
        return Arrays.stream(SuitType.values()).map(s -> new CardsUnDealtPerSuitDTO(s, getTotalCardsBySuitType(s, game))).toList();
    }

    private static int getTotalCardsBySuitType(SuitType s, PokerGame game) {
        return game.getDecks().stream()
                .mapToInt(d -> Math.toIntExact(d.getCards().stream().filter(c -> c.getSuit().equals(s)).count())).sum();
    }

}
