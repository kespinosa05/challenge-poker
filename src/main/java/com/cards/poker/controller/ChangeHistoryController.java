package com.cards.poker.controller;

import com.cards.poker.dto.PokerGameEventDTO;
import com.cards.poker.model.event.PokerGameEntityType;
import com.cards.poker.service.PokerGameChangeListener;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/history")
public class ChangeHistoryController {

    private final PokerGameChangeListener gameChangeListener;

    @GetMapping("/event")
    public ResponseEntity<List<PokerGameEventDTO>> getChangeHistory(@RequestParam PokerGameEntityType entity) {
        return ResponseEntity.status(HttpStatus.OK).body(gameChangeListener.getEventsPerEntityType(entity));
    }
}
