package com.cards.poker.controller;

import com.cards.poker.dto.CardsUnDealtPerSuitDTO;
import com.cards.poker.dto.PlayerTotalValueDTO;
import com.cards.poker.model.Card;
import com.cards.poker.service.PokerGameService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/games")
public class PokerGameController {

    private final PokerGameService pokerGameService;

    @PostMapping("/create")
    public ResponseEntity<Void> createGame(@RequestBody @Valid String name) {
        pokerGameService.createGame(name);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{gameName}")
    public ResponseEntity<Void> deleteGame(@PathVariable("gameName") String gameName) {
        pokerGameService.deleteGame(gameName);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{gameName}/decks/{deckId}")
    public ResponseEntity<Void> addDeckToGame(@PathVariable("gameName") String gameName, @PathVariable("deckId") UUID deckId) {
        pokerGameService.addDeckToGame(gameName, deckId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/{gameName}/players/")
    public ResponseEntity<Void> addPlayerToGame(@PathVariable("gameName") String gameName,
                                                @RequestBody @Valid String playerName) {
        pokerGameService.addPlayer(gameName, playerName);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{gameName}/players/{playerName}")
    public ResponseEntity<Void> removePlayerToGame(@PathVariable("gameName") String gameName,
                                                   @PathVariable("playerName") String playerName) {
        pokerGameService.removePlayer(gameName, playerName);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{gameName}/players/{playerName}/deal/{quantity}")
    public ResponseEntity<Void> dealCards(@PathVariable("gameName") String gameName,
                                          @PathVariable("playerName") String playerName,
                                          @PathVariable("quantity") Integer quantity) {
        pokerGameService.dealCards(gameName, playerName, quantity);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{gameName}/players/{playerName}/hand")
    public ResponseEntity<List<Card>> getPlayerHand(@PathVariable("gameName") String gameName,
                                                    @PathVariable("playerName") String playerName) {
        return ResponseEntity.status(HttpStatus.OK).body(pokerGameService.getPlayerHand(gameName, playerName));
    }

    @GetMapping("/{gameName}/totalRankPerPlayer")
    public ResponseEntity<List<PlayerTotalValueDTO>> totalRankPerPlayer(@PathVariable("gameName") String gameName) {
        return ResponseEntity.status(HttpStatus.OK).body(pokerGameService.totalRankPerPlayer(gameName));
    }

    @GetMapping("/{gameName}/cardsUnDealt")
    public ResponseEntity<List<CardsUnDealtPerSuitDTO>> cardsUnDealtPerSuit(@PathVariable("gameName") String gameName) {
        return ResponseEntity.status(HttpStatus.OK).body(pokerGameService.cardsUnDealtPerSuit(gameName));
    }

}
