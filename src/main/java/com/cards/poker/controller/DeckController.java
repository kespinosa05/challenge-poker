package com.cards.poker.controller;

import com.cards.poker.service.DeckService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/decks")
public class DeckController {

    private final DeckService deckService;

    @PostMapping("/create")
    public ResponseEntity<UUID> create() {
        return ResponseEntity.ok(deckService.createNewDeck().getId());
    }


}
