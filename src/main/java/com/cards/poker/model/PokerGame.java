package com.cards.poker.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PokerGame {
    private String name;

    @Builder.Default
    private Set<Player> players = new HashSet<>();

    @Builder.Default
    private Set<Deck> decks = new HashSet<>();
}
