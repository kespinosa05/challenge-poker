package com.cards.poker.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Deck {
    private static final Random RANDOM = new SecureRandom();

    private UUID id;

    @Builder.Default
    private Set<Card> cards = new HashSet<>();

    public synchronized Card retriveCard() {
        if (cards.isEmpty()) {
            return null;
        }
        var card = cards.stream().skip(RANDOM.nextInt(cards.size())).findFirst().orElseThrow();
        cards.remove(card);
        return card;
    }


}
