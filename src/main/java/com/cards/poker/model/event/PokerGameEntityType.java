package com.cards.poker.model.event;

public enum PokerGameEntityType {
    GAME,
    PLAYER,
    DECK
}
