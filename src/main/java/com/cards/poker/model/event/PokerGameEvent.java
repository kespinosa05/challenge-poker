package com.cards.poker.model.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.List;
import java.util.Map;

@Getter
public class PokerGameEvent extends ApplicationEvent {
    private final List<PokerGameEntityType> entities;
    private final String action;
    private final transient Map<String, Object> details;

    public PokerGameEvent(Object source, List<PokerGameEntityType> entities, String action, Map<String, Object> details) {
        super(source);
        this.entities = entities;
        this.action = action;
        this.details = details;
    }

}
