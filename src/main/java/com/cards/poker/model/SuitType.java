package com.cards.poker.model;

public enum SuitType {
    HEART,
    SPADE,
    CLUB,
    DIAMOND
}
