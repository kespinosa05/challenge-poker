package com.cards.poker.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Player {

    private String name;

    @Builder.Default
    private Set<Card> hand = new HashSet<>();

    public Integer getTotalValue() {
        return hand.stream().mapToInt(c -> c.getFace().getRank()).sum();
    }

}
