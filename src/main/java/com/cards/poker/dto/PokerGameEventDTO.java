package com.cards.poker.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class PokerGameEventDTO {
    private LocalDateTime date;
    private String action;
    private Map<String, Object> details;

}
