package com.cards.poker.dto;

import com.cards.poker.model.SuitType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CardsUnDealtPerSuitDTO {
    private SuitType suit;
    private Integer quantity;
}
