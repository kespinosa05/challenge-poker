package com.cards.poker.controller;

import com.cards.poker.model.SuitType;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.datafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class PokerGameControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private Faker faker = new Faker();


    @Test
    public void createGame() throws Exception {
        requestCreateGame(faker.name().name())
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteGame() throws Exception {
        String name = faker.name().name();
        requestCreateGame(name);
        mockMvc.perform(delete("/games/" + name)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    public void addDeckToGame() throws Exception {
        String name = faker.name().name();
        requestCreateGame(name);
        UUID deck = requestCreateDeck();
        requestAddDeck(name, deck)
                .andExpect(status().isCreated());
    }

    @Test
    public void addPlayerToGame() throws Exception {
        String name = faker.name().name();
        requestCreateGame(name);
        var playerName = faker.name().name();
        requestAddPlayer(name, playerName)
                .andExpect(status().isCreated());
    }


    @Test
    public void dealCards() throws Exception {
        var playerName = faker.name().name();
        var amountOfCards = faker.number().randomDigitNotZero();
        String name = faker.name().name();
        requestCreateGame(name);
        requestAddPlayer(name, playerName);
        UUID deck = requestCreateDeck();
        requestAddDeck(name, deck);
        requestDealCard(name, playerName, amountOfCards)
                .andExpect(status().isNoContent());
    }

    @Test
    public void getHand() throws Exception {
        var playerName = faker.name().name();
        var cards = faker.number().randomDigit();
        var name = faker.name().name();
        requestCreateGame(name);
        requestAddPlayer(name, playerName);
        UUID deck = requestCreateDeck();
        requestAddDeck(name, deck);
        requestDealCard(name, playerName, cards);
        mockMvc.perform(get("/games/" + name + "/players/" + playerName + "/hand")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(cards));
    }

    @Test
    public void getTotalRankPerPlayer() throws Exception {
        var name = faker.name().name();
        requestCreateGame(name);
        requestAddPlayer(name, faker.name().name());
        requestAddPlayer(name, faker.name().name());

        mockMvc.perform(get("/games/" + name + "/totalRankPerPlayer")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void cardsUnDealt() throws Exception {
        var playerName = faker.name().name();
        var cards = faker.number().randomDigit();
        var name = faker.name().name();
        requestCreateGame(name);
        requestAddPlayer(name, playerName);
        UUID deck = requestCreateDeck();
        requestAddDeck(name, deck);
        requestDealCard(name, playerName, cards);

        mockMvc.perform(get("/games/" + name + "/cardsUnDealt")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(SuitType.values().length));
    }


    private ResultActions requestCreateGame(String name) throws Exception {
        return mockMvc.perform(post("/games/create")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(name));
    }

    private ResultActions requestAddPlayer(String name, String playerName) throws Exception {
        return mockMvc.perform(post("/games/" + name + "/players/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(playerName));
    }

    private ResultActions requestAddDeck(String name, UUID deck) throws Exception {
        return mockMvc.perform(post("/games/" + name + "/decks/" + deck)
                .contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    private UUID requestCreateDeck() throws Exception {
        String response = mockMvc.perform(post("/decks/create").contentType(MediaType.APPLICATION_JSON_VALUE).characterEncoding("")).andReturn().getResponse().getContentAsString();
        UUID deck = new ObjectMapper().readValue(response, UUID.class);
        return deck;
    }

    private ResultActions requestDealCard(String name, String playerName, int amountOfCards) throws Exception {
        return mockMvc.perform(post("/games/" + name + "/players/" + playerName + "/deal/" + amountOfCards)
                .contentType(MediaType.APPLICATION_JSON_VALUE));
    }

}