package com.cards.poker;

import com.cards.poker.service.DeckService;
import com.cards.poker.service.PokerGameService;
import net.datafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PokerGameApplicationTests {

    private Faker faker = new Faker();

    @Autowired
    private DeckService deckService;
    @Autowired
    private PokerGameService pokerGameService;


    @Test
    public void testApplicationStarts() {
        PokerGameApplication.main();

        assertNotNull(deckService);
        assertNotNull(pokerGameService);
    }

}