package com.cards.poker.exception;

import net.datafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class PokerGameRestControllerAdviceTest {

    private final Faker faker = new Faker();

    private PokerGameRestControllerAdvice pokerGameRestControllerAdvice = new PokerGameRestControllerAdvice();

    @Test
    void testPokerGameBaseException() {
        ErrorResponse errorResponse = pokerGameRestControllerAdvice.pokerGameBaseException(
                new DeckAlreadyExistInPokerGameException());
        assertNotNull(errorResponse);
        assertEquals(DeckAlreadyExistInPokerGameException.class.getSimpleName(), errorResponse.getErrorType());
        assertNull(errorResponse.getMessage());
    }

    @Test
    void testGenericExceptionHandler() {
        var errorMessage = faker.address().fullAddress();
        ErrorResponse errorResponse = pokerGameRestControllerAdvice.genericExceptionHandler(new NullPointerException(errorMessage));
        assertNotNull(errorResponse);
        assertEquals(NullPointerException.class.getSimpleName(), errorResponse.getErrorType());
        assertEquals(errorMessage, errorResponse.getMessage());
    }
}