package com.cards.poker.controller;

import com.cards.poker.dto.PokerGameEventDTO;
import com.cards.poker.model.event.PokerGameEntityType;
import com.cards.poker.service.PokerGameChangeListener;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ChangeHistoryControllerTest {

    @InjectMocks
    private ChangeHistoryController changeHistoryController;

    @Mock
    private PokerGameChangeListener pokerGameChangeListener;

    @Test
    void testGetChangeHistory() {
        PokerGameEntityType game = PokerGameEntityType.GAME;
        var events = List.of(PokerGameEventDTO.builder().build());

        when(pokerGameChangeListener.getEventsPerEntityType(game)).thenReturn(events);

        var response = changeHistoryController.getChangeHistory(game);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(events.size(), response.getBody().size());
        verify(pokerGameChangeListener).getEventsPerEntityType(game);
    }
}