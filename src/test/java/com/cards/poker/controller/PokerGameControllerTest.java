package com.cards.poker.controller;

import com.cards.poker.dto.CardsUnDealtPerSuitDTO;
import com.cards.poker.dto.PlayerTotalValueDTO;
import com.cards.poker.model.Card;
import com.cards.poker.model.Player;
import com.cards.poker.service.PokerGameService;
import net.datafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class PokerGameControllerTest {

    @InjectMocks
    private PokerGameController pokerGameController;

    @Mock
    private PokerGameService pokerGameService;

    private final Faker faker = new Faker();

    @Test
    void testCreateGame() {
        var name = faker.name().name();
        when(pokerGameService.createGame(name)).thenReturn(any());
        var response = pokerGameController.createGame(name);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(pokerGameService).createGame(name);
    }

    @Test
    void testDeleteGame() {
        var name = faker.name().name();
        doNothing().when(pokerGameService).deleteGame(name);
        var response = pokerGameController.deleteGame(name);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(pokerGameService).deleteGame(name);
    }

    @Test
    void testAddDeckToGame() {
        var name = faker.name().name();
        var deckId = UUID.randomUUID();
        doNothing().when(pokerGameService).addDeckToGame(name, deckId);
        var response = pokerGameController.addDeckToGame(name, deckId);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(pokerGameService).addDeckToGame(name, deckId);
    }

    @Test
    void testAddPlayerToGame() {
        var gameName = faker.name().name();
        var payerName = faker.name().name();
        when(pokerGameService.addPlayer(gameName, payerName)).thenReturn(Player.builder().build());
        var response = pokerGameController.addPlayerToGame(gameName, payerName);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(pokerGameService).addPlayer(gameName, payerName);
    }

    @Test
    void testRemovePlayerToGame() {
        var gameName = faker.name().name();
        var payerName = faker.name().name();
        doNothing().when(pokerGameService).removePlayer(gameName, payerName);
        var response = pokerGameController.removePlayerToGame(gameName, payerName);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(pokerGameService).removePlayer(gameName, payerName);
    }

    @Test
    void testDealCards() {
        var gameName = faker.name().name();
        var payerName = faker.name().name();
        Integer quantity = faker.number().randomDigitNotZero();
        doNothing().when(pokerGameService).dealCards(gameName, payerName, quantity);
        var response = pokerGameController.dealCards(gameName, payerName, quantity);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(pokerGameService).dealCards(gameName, payerName, quantity);
    }

    @Test
    void testGetPlayerHand() {
        var gameName = faker.name().name();
        var payerName = faker.name().name();
        var result = List.of(Card.builder().build());
        when(pokerGameService.getPlayerHand(gameName, payerName)).thenReturn(result);

        var response = pokerGameController.getPlayerHand(gameName, payerName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(result.size(), response.getBody().size());
        verify(pokerGameService).getPlayerHand(gameName, payerName);
    }

    @Test
    void testTotalRankPerPlayer() {
        var gameName = faker.name().name();
        var result = List.of(PlayerTotalValueDTO.builder().build());
        when(pokerGameService.totalRankPerPlayer(gameName)).thenReturn(result);

        var response = pokerGameController.totalRankPerPlayer(gameName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(result.size(), response.getBody().size());
        verify(pokerGameService).totalRankPerPlayer(gameName);
    }

    @Test
    void testCardsUnDealt() {
        var gameName = faker.name().name();
        var result = List.of(CardsUnDealtPerSuitDTO.builder().build());
        when(pokerGameService.cardsUnDealtPerSuit(gameName)).thenReturn(result);

        var response = pokerGameController.cardsUnDealtPerSuit(gameName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(result.size(), response.getBody().size());
        verify(pokerGameService).cardsUnDealtPerSuit(gameName);
    }


}