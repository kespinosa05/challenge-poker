package com.cards.poker.service;

import com.cards.poker.exception.DeckNotFoundException;
import com.cards.poker.model.Deck;
import com.cards.poker.model.event.PokerGameEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static com.cards.poker.model.event.PokerGameEntityType.DECK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class DeckServiceTest {
    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Mock
    private MetricService metricService;

    @InjectMocks
    private DeckService deckService;

    @Test
    void testCreateNewDeck() {
        doNothing().when(metricService).deckCreated();

        doNothing().when(eventPublisher).publishEvent(any(PokerGameEvent.class));
        var deckCreated = deckService.createNewDeck();
        assertEquals(52, deckCreated.getCards().size());

        verify(eventPublisher).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().contains(DECK)));
        verify(metricService, times(1)).deckCreated();
    }

    @Test
    void testGetDeck() {
        doNothing().when(eventPublisher).publishEvent(any(PokerGameEvent.class));
        var deckCreated = deckService.createNewDeck();
        Deck deck = deckService.getDeck(deckCreated.getId());
        assertEquals(deckCreated.getId(), deck.getId());
    }

    @Test
    void testGetDeckThrowDeckNotFoundException() {
        var id = UUID.randomUUID();
        assertThrows(DeckNotFoundException.class, () -> {
            deckService.getDeck(id);
        });
    }
}