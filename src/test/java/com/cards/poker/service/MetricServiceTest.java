package com.cards.poker.service;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import net.datafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.cards.poker.service.MetricService.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class MetricServiceTest {
    @Mock
    private MeterRegistry registry;

    @InjectMocks
    private MetricService metricService;

    private Faker faker = new Faker();

    @Test
    void gameCreated() {
        var counter = mock(Counter.class);
        when(registry.counter(COUNTER_GAME_CREATED)).thenReturn(counter);

        metricService.gameCreated();

        verify(registry, times(1)).counter(COUNTER_GAME_CREATED);
        verify(counter, times(1)).increment();
    }

    @Test
    void testPlayerAdded() {
        var gameName = faker.name().name();
        var playerName = faker.name().name();
        var counter = mock(Counter.class);
        var tags = List.of(Tag.of("gameName", gameName), Tag.of("playerName", playerName));
        when(registry.counter(COUNTER_GAME_PLAYERS_ADDED, tags)).thenReturn(counter);

        metricService.playerAdded(gameName, playerName);

        verify(registry, times(1)).counter(COUNTER_GAME_PLAYERS_ADDED, tags);
        verify(counter, times(1)).increment();
    }

    @Test
    void testPlayerRemoved() {
        var gameName = faker.name().name();
        var playerName = faker.name().name();
        var counter = mock(Counter.class);
        var tags = List.of(Tag.of("gameName", gameName), Tag.of("playerName", playerName));
        when(registry.counter(COUNTER_GAME_PLAYERS_REMOVED, tags)).thenReturn(counter);

        metricService.playerRemoved(gameName, playerName);

        verify(registry, times(1)).counter(COUNTER_GAME_PLAYERS_REMOVED, tags);
        verify(counter, times(1)).increment();
    }

    @Test
    void testDeal() {
        var gameName = faker.name().name();
        var playerName = faker.name().name();
        var quantity = faker.number().positive();
        var counter = mock(Counter.class);
        var tags = List.of(Tag.of("gameName", gameName), Tag.of("playerName", playerName));
        when(registry.counter(COUNTER_GAME_DEAL_CARD, tags)).thenReturn(counter);

        metricService.deal(gameName, playerName, quantity);

        verify(registry, times(1)).counter(COUNTER_GAME_DEAL_CARD, tags);
        verify(counter, times(1)).increment(quantity);
    }

    @Test
    void testDeckCreated() {
        var counter = mock(Counter.class);
        when(registry.counter(COUNTER_DECK_CREATED)).thenReturn(counter);

        metricService.deckCreated();

        verify(registry, times(1)).counter(COUNTER_DECK_CREATED);
        verify(counter, times(1)).increment();
    }
}