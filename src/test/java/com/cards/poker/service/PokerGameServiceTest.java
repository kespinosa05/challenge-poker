package com.cards.poker.service;

import com.cards.poker.exception.*;
import com.cards.poker.model.Deck;
import com.cards.poker.model.SuitType;
import com.cards.poker.model.event.PokerGameEvent;
import net.datafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static com.cards.poker.model.event.PokerGameEntityType.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class PokerGameServiceTest {

    private static final Integer MAX_QUANTITY_CARD = 52;

    @Mock
    private DeckService deckService;
    @Mock
    private MetricService metricService;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @InjectMocks
    private PokerGameService pokerGameService;

    private final Faker faker = new Faker();

    private Deck deck = Deck.builder()
            .id(UUID.randomUUID())
            .cards(DeckService.generateDeckCards())
            .build();

    @BeforeEach
    public void onEachTest() {
        when(deckService.createNewDeck()).thenReturn(deck);
        when(deckService.getDeck(deck.getId())).thenReturn(deck);
    }


    @Test
    void testCreateGame() {
        var name = faker.name().name();
        var gameCreated = pokerGameService.createGame(name);
        assertEquals(name, gameCreated.getName());

        verify(eventPublisher).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().contains(GAME)));
        verify(metricService).gameCreated();
    }

    @Test
    void testCreateGameThrowsGameAlreadyExistException() {
        var name = faker.name().name();
        pokerGameService.createGame(name);

        assertThrows(GameAlreadyExistException.class, () -> pokerGameService.createGame(name));
    }

    @Test
    void testDeleteGameThrowGameNotFoundException() {
        var name = faker.name().name();

        assertThrows(GameNotFoundException.class, () -> pokerGameService.deleteGame(name));
    }

    @Test
    void testDeleteGameThrowsGameNotFoundException() {
        var name = faker.name().name();
        pokerGameService.createGame(name);
        pokerGameService.deleteGame(name);

        verify(eventPublisher, times(2)).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().contains(GAME)));
    }

    @Test
    void testAddDeckToGame() {
        var name = faker.name().name();
        var deck = deckService.createNewDeck();
        pokerGameService.createGame(name);

        pokerGameService.addDeckToGame(name, deck.getId());

        verify(eventPublisher, times(2)).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().contains(GAME)));
        verify(eventPublisher, times(1)).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().containsAll(List.of(GAME, DECK))));
    }

    @Test
    void testAddDeckToGameThrowDeckAlreadyExistInPokerGameException() {
        var name = faker.name().name();
        var deck = deckService.createNewDeck();
        var id = deck.getId();

        pokerGameService.createGame(name);

        pokerGameService.addDeckToGame(name, id);

        assertThrows(DeckAlreadyExistInPokerGameException.class, () -> pokerGameService.addDeckToGame(name, id));
    }

    @Test
    void testAddDeckToGameThrowGameNotFoundException() {
        var name = faker.name().name();
        var id = deck.getId();
        assertThrows(GameNotFoundException.class, () -> {
            pokerGameService.addDeckToGame(name, id);
        });
    }


    @Test
    void testAddPlayerToGame() {
        var name = faker.name().name();
        var playerName = faker.name().name();

        pokerGameService.createGame(name);

        var player = pokerGameService.addPlayer(name, playerName);
        assertEquals(playerName, player.getName());

        verify(eventPublisher).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().containsAll(List.of(GAME, PLAYER))));
        verify(metricService).playerAdded(name, playerName);
    }

    @Test
    void testAddPlayerToGameThrowPlayerAlreadyExistException() {
        var name = faker.name().name();
        var playerName = faker.name().name();

        pokerGameService.createGame(name);

        pokerGameService.addPlayer(name, playerName);

        assertThrows(PlayerAlreadyExistException.class, () -> pokerGameService.addPlayer(name, playerName));
    }

    @Test
    void testRemovePlayerToGame() {
        var name = faker.name().name();
        var playerName = faker.name().name();

        pokerGameService.createGame(name);
        pokerGameService.addPlayer(name, playerName);

        pokerGameService.removePlayer(name, playerName);

        verify(eventPublisher, times(2)).publishEvent(argThat((PokerGameEvent event) -> event.getEntities().containsAll(List.of(GAME, PLAYER))));
        verify(metricService).playerRemoved(name, playerName);
    }

    @Test
    void testRemovePlayerToGameThrowPlayerNotFoundException() {
        var name = faker.name().name();
        var playerName = faker.name().name();

        pokerGameService.createGame(name);

        assertThrows(PlayerNotFoundException.class, () -> pokerGameService.removePlayer(name, playerName));
    }

    @Test
    void testDealCards() {
        var gameName = faker.name().name();
        var player1Name = faker.name().name();
        var quantity = faker.number().numberBetween(1, MAX_QUANTITY_CARD);
        pokerGameService.createGame(gameName);
        var deck = deckService.createNewDeck();
        pokerGameService.addDeckToGame(gameName, deck.getId());

        var playerCreated = pokerGameService.addPlayer(gameName, player1Name);

        pokerGameService.dealCards(gameName, player1Name, quantity);

        assertEquals(quantity, playerCreated.getHand().size());
        verify(metricService).deal(gameName, player1Name, (int) quantity);
    }

    @Test
    void testDealCardsWithMoreThanMaxCardAllowed() {
        var gameName = faker.name().name();
        var player1Name = faker.name().name();
        var quantity = MAX_QUANTITY_CARD + 1;
        pokerGameService.createGame(gameName);
        var deck = deckService.createNewDeck();
        pokerGameService.addDeckToGame(gameName, deck.getId());

        var playerCreated = pokerGameService.addPlayer(gameName, player1Name);

        pokerGameService.dealCards(gameName, player1Name, quantity);

        assertEquals(MAX_QUANTITY_CARD, playerCreated.getHand().size());
    }

    @Test
    void testDealCardsThrowDeckNotFoundException() {
        var gameName = faker.name().name();
        var player1Name = faker.name().name();
        var quantity = faker.number().numberBetween(1, MAX_QUANTITY_CARD);
        pokerGameService.createGame(gameName);

        pokerGameService.addPlayer(gameName, player1Name);

        assertThrows(DeckNotFoundException.class, () -> pokerGameService.dealCards(gameName, player1Name, (int) quantity));
    }

    @Test
    void testGetPlayerHand() {
        var gameName = faker.name().name();
        var player1Name = faker.name().name();
        var quantity = faker.number().numberBetween(1, MAX_QUANTITY_CARD);
        pokerGameService.createGame(gameName);
        var deck = deckService.createNewDeck();
        pokerGameService.addDeckToGame(gameName, deck.getId());

        pokerGameService.addPlayer(gameName, player1Name);

        pokerGameService.dealCards(gameName, player1Name, quantity);

        var hand = pokerGameService.getPlayerHand(gameName, player1Name);

        assertEquals(quantity, hand.size());
    }


    @Test
    void testTotalRankPerPlayer() {
        var gameName = faker.name().name();

        var player1Name = faker.name().name();
        var player2Name = faker.name().name();
        var totalPlayers = 1;
        var quantityPlayer1 = faker.number().numberBetween(1, MAX_QUANTITY_CARD);
        var quantityPlayer2 = faker.number().numberBetween(1, MAX_QUANTITY_CARD - quantityPlayer1);
        pokerGameService.createGame(gameName);
        var deck = deckService.createNewDeck();
        pokerGameService.addDeckToGame(gameName, deck.getId());

        pokerGameService.addPlayer(gameName, player1Name);
        pokerGameService.addPlayer(gameName, player2Name);

        pokerGameService.dealCards(gameName, player1Name, quantityPlayer1);

        if (quantityPlayer1 <= MAX_QUANTITY_CARD) {
            pokerGameService.dealCards(gameName, player2Name, quantityPlayer2);
            totalPlayers++;
        }

        var totalRankPerPlayer = pokerGameService.totalRankPerPlayer(gameName);

        assertEquals(totalPlayers, totalRankPerPlayer.size());
    }

    @Test
    void testCardsUnDealtPerSuit() {
        var gameName = faker.name().name();
        var player1Name = faker.name().name();
        var player2Name = faker.name().name();
        var quantityPlayer1 = faker.number().numberBetween(1, MAX_QUANTITY_CARD);
        var quantityPlayer2 = faker.number().numberBetween(1, MAX_QUANTITY_CARD - quantityPlayer1);
        pokerGameService.createGame(gameName);
        var deck = deckService.createNewDeck();
        pokerGameService.addDeckToGame(gameName, deck.getId());

        pokerGameService.addPlayer(gameName, player1Name);
        pokerGameService.addPlayer(gameName, player2Name);

        pokerGameService.dealCards(gameName, player1Name, quantityPlayer1);
        if (quantityPlayer1 <= MAX_QUANTITY_CARD) {
            pokerGameService.dealCards(gameName, player2Name, quantityPlayer2);
        }

        var cardsUnDealtPerSuit = pokerGameService.cardsUnDealtPerSuit(gameName);

        assertEquals(SuitType.values().length, cardsUnDealtPerSuit.size());
    }


}