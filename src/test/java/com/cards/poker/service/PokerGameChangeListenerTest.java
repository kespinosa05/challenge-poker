package com.cards.poker.service;

import com.cards.poker.model.event.PokerGameEntityType;
import com.cards.poker.model.event.PokerGameEvent;
import net.datafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class PokerGameChangeListenerTest {

    @InjectMocks
    private PokerGameChangeListener pokerGameChangeListener;

    private final Faker faker = new Faker();

    @Test
    void handleCustomEvent() {
        var pokerGameEvent = new PokerGameEvent(this, List.of(PokerGameEntityType.GAME), faker.name().name(), Collections.emptyMap());

        pokerGameChangeListener.handleCustomEvent(pokerGameEvent);

        var result = pokerGameChangeListener.getEventsPerEntityType(PokerGameEntityType.GAME);

        assertEquals(1, result.size());


    }


}