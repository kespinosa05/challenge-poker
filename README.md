## Poker Game API ##

This API has the following requirements:

- Create and delete a game
- Create a deck
- Add a deck to a game deck
    - Please note that once a deck has been added to a game deck it cannot be removed.
- Add and remove players from a game
- Deal cards to a player in a game from the game deck
    - Specifically, for a game deck containing only one deck of cards, a call to shuffle followed by 52 calls to
      dealCards(1)
      for the same player should result in the caller being provided all 52 cards of the deck in a random order. If the
      caller
      then makes a 53rd call to dealCard(1), no card is dealt. This approach is to be followed if the game deck contains
      more than one deck.
- Get the list of cards for a player
- Get the list of players in a game along with the total added value of all the cards each player holds; use face
  values of cards only.
  Then sort the list in descending order, from the player with the highest value hand to the player with the lowest
  value hand:
    - For instance if player ‘A’ holds a 10 + King then her total value is 23 and player ‘B’ holds a 7 + Queen then his
      total
      value is 19, so player ‘A’ will be listed first followed by player ‘B’.
- Get the count of how many cards per suit are left undealt in the game deck (example: 5 hearts, 3 spades, etc.)
- The Datastore should be In-Memory and support concurrency access for the purpose of
  this exercise DO NOT USE existing Java ConcurrentHashMap, use a standard HashMap and build concurrency support around
  it.
- An event should be triggered when an action is performed, include the change that
  occurred. The listener should store an history of changes which will be exposed via an
  endpoint that list out the Event in chronological order, for any specified entity (Game,
  Player, Deck)

### Architecture ###
- Gradle is used as build tool
- Spring boot 3.1.4 is used as framework
- The architecture manages different http status
  - Http Status 200 and 201: for valid requests
  - Http Status 400: for invalid requests
  - Http  Status 500: for unexpected exceptions

### Dependencies ###

- `Java 17`
- `Gradle 8.0+` or gradle wrapper `./gradlew`
- `Docker` To build and run the app as container

### Running the API ###

- Using gradle: ```gradle bootRun```
- Using gradle wrapper: ```./gradlew bootRun```
- Using docker ```docker run -it --name poker-game -p 8080:8080 poker-game:1.0.0```

### Running unit test ###

Run command ```./gradlew test```

### Running integration test ###

Run command ```./gradlew integrationTest```

### Running mutation test ###

Run command ``` ./gradlew pitest```

### Code coverage and quality ###

Run command ```./gradlew sonar``` to update sonar server instance or run ```./gradlew jacocoLogTestCoverage``` to see it
in the console

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=kespinosa05_challenge-poker&metric=coverage)](https://sonarcloud.io/component_measures?id=kespinosa05_challenge-poker&metric=coverage&view=list)

[![Technical deb](https://sonarcloud.io/api/project_badges/measure?project=kespinosa05_challenge-poker&metric=sqale_index)](https://sonarcloud.io/component_measures?id=kespinosa05_challenge-poker&metric=sqale_index&view=list)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=kespinosa05_challenge-poker&metric=vulnerabilities)](https://sonarcloud.io/component_measures?id=kespinosa05_challenge-poker&metric=vulnerabilities&view=list)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=kespinosa05_challenge-poker&metric=bugs)](https://sonarcloud.io/component_measures?id=kespinosa05_challenge-poker&metric=bugs&view=list)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=kespinosa05_challenge-poker&metric=code_smells)](https://sonarcloud.io/component_measures?id=kespinosa05_challenge-poker&metric=code_smells&view=list)

[![Security rating](https://sonarcloud.io/api/project_badges/measure?project=kespinosa05_challenge-poker&metric=security_rating)](https://sonarcloud.io/component_measures?id=kespinosa05_challenge-poker&metric=security_rating&view=list)

### Check OWASP known dependencies vulnerabilities  ###

Run command ```./gradlew dependencyCheckAnalyze```

### Check known dependencies vulnerabilities with Snyk check ###

- Using snyk tool, First must install snyk tool and run command ```snyk auth``` then run command ```snyk test```
- Using gradle plugin, First must set an environment variable SNYK_TOKEN then run command ./gradlew snyk-test

### Check docker container vulnerabilities ###

Docker image should be create first,
then run command ```snyk container test poker-game:1.0.0```

### Check known security issues using Static Code Analysis ###

Run command ```snyk code test```

**_NOTE:_** There is 2 low security issue related to CSRF due to not using Spring Security

### Open Api specification ###

http://localhost:8080/pokerGameApi/swagger-ui.html

http://localhost:8080/pokerGameApi/api-docs

### Metrics ###

Metrics are exposed on prometheus format

http://localhost:8080/pokerGameApi/actuator/prometheus

There is a few custom metric 
- <b>counter_game_created_total</b> to count game created
- <b>counter_deck_created_total</b> to count deck created
- <b>counter_game_player_added_total</b> to count player added in a game
- <b>counter_game_player_removed_total</b> to count player removed in a game
- <b>counter_game_dealcard_total</b> to count deal card in a game

### Build docker image ###

- Using docker ```docker build -t poker-game:1.0.0 .```

- Using gradle ```./gradlew bootBuildImage --imageName=poker-game:1.0.0```

### Running on k8s ###

**_NOTE:_** A Kubernetes cluster configured is required.

#### Package ####

- Helm chart can be packaged using this command ```helm package ./helm``` and then uploaded to a repository

#### Install ####

- Using package run this command  ```helm install poker-game poker-game-0.1.0.tgz -n default```
- Using folder directory directly run this command ```helm install  poker-game ./helm -n default```

#### Update or Install ####

- Using package run this command  ```helm upgrade poker-game poker-game-0.1.0.tgz --install -n default```
- Using folder directory directly run this command helm ```upgrade poker-game ./helm -n default```

#### Uninstall ####

Run this command ```helm uninstall poker-game -n default```








